package entity;

import java.util.Locale;

import entity.util.Alerts;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CalculoBoloController {

	@FXML
	private TextField pesoText;

	@FXML
	private TextField valorText;

	@FXML
	private Button btnCalculo;

	@FXML
	private Label resultLabel;
		
	@FXML
	public void btnCalcularAction() {
		try {
			Locale.setDefault(Locale.US);
			double pesoBolo = Double.parseDouble(pesoText.getText());
			double valorBolo = Double.parseDouble(valorText.getText());
			double valor = (pesoBolo * valorBolo) / 1000;
			resultLabel.setText(String.format("%.2f", valor));
		}catch(NumberFormatException e) {
			Alerts.showAlert("Error", "Parse error", e.getMessage(), AlertType.ERROR);
		}
	}
		
}
